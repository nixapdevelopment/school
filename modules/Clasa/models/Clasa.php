<?php

namespace app\modules\Clasa\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "Clasa".
 *
 * @property integer $ID
 * @property integer $SchoolID
 * @property integer $StartYear
 * @property integer $EndYear
 * @property integer $Number
 * @property string $Letter
 * @property string $Description
 * @property string $Type
 * @property string $Status
 */
class Clasa extends \yii\db\ActiveRecord
{
    
    const TypeReal = 'Real';
    const TypeUman = 'Uman';
    
    const StatusActive = 'Active';
    const StatusDeleted = 'Deleted';
    
    
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'CreatedBy',
                'updatedByAttribute' => false,
            ],
        ];
    }

    public static function getTypesList()
    {
        return [
            '' => '-',
            self::TypeReal => 'Real',
            self::TypeUman => 'Uman',
        ];
    }

    public static function getStatusList()
    {
        return [
            '' => '-',
            self::StatusActive => 'Active',
            self::StatusDeleted => 'Deleted',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Clasa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['SchoolID', 'default', 'value' => 1],
            ['Status', 'default', 'value' => Clasa::StatusActive],
            [['SchoolID', 'StartYear', 'EndYear', 'Number'], 'integer'],
            [['StartYear', 'EndYear', 'Number', 'Type', 'Status'], 'required'],
            [['Description'], 'string'],
            [['Letter', 'Type'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SchoolID' => Yii::t('app', 'School ID'),
            'StartYear' => Yii::t('app', 'Anul admiterei'),
            'EndYear' => Yii::t('app', 'Anul absolvirii'),
            'Number' => Yii::t('app', 'Numar'),
            'Letter' => Yii::t('app', 'Litera'),
            'Description' => Yii::t('app', 'Descriere'),
            'Type' => Yii::t('app', 'Profil'),
        ];
    }
    
    public static function getClassNumbers($addEmptyValue = false)
    {
        $return = $addEmptyValue ? ['' => '-'] : [];
        
        $return += [
            0 => 0,
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10,
            11 => 11,
            12 => 12,
        ];
        
        return $return;
    }
    
    public static function getClassLetters($addEmptyValue = false)
    {
        $return = $addEmptyValue ? ['' => '-'] : [];
        
        $alphas = range('A', 'Z');
        
        foreach ($alphas as $alpha)
        {
            $return[$alpha] = $alpha;
        }
        
        return $return;
    }
    
    public static function getClassesList($addEmptyValue = false)
    {
        $classes = Clasa::findAll(['Status' => Clasa::StatusActive]);
        
        $return = $addEmptyValue ? ['' => '-'] : [];
        
        foreach ($classes as $class)
        {
            $return[$class->ID] = $class->Number . $class->Letter;
        }
        
        return $return;
    }
    
    public function getFullName()
    {
        return $this->Number . $this->Letter;
    }
    
}
