<?php

namespace app\components\CDatepicker;

use kartik\date\DatePicker;

class CDatepicker extends DatePicker
{
    
    public $type = self::TYPE_COMPONENT_APPEND;

    public $pluginOptions = [
        'autoclose' => true,
        'format' => 'dd.mm.yyyy',
        'todayHighlight' => true,
    ];
    
}
