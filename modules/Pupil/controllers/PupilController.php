<?php

namespace app\modules\Pupil\controllers;

use app\models\User\User;
use Yii;
use app\modules\Pupil\models\Pupil;
use app\modules\Pupil\models\PupilSearch;
use app\modules\Pupil\controllers\DefaultController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\Clasa\models\Clasa;

/**
 * PupilController implements the CRUD actions for Pupil model.
 */
class PupilController extends DefaultController
{
    public $parsed = [];
    public $skiped = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pupil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PupilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pupil model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pupil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pupil();
        $model->event = 'add';
        $classes = Clasa::find()->where(['Status' => Clasa::StatusActive])->indexBy('ID')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            Yii::$app->session->setFlash('success', 'Date salvate');
            return $this->redirect(['update', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'classes' => $classes,
            ]);
        }
    }

    /**
     * Updates an existing Pupil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $classes = Clasa::find()->where(['Status' => Clasa::StatusActive])->indexBy('ID')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Date salvate');
            return $this->redirect(['index', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'classes' => $classes,
            ]);
        }
    }

    /**
     * Deletes an existing Pupil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pupil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pupil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pupil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionImport()
    {   $classes = Clasa::getClassesList($addEmptyValue = false);

        if (Yii::$app->request->isPost)
        {

            if (($handle = fopen($_FILES['File']['tmp_name'], "r")) !== FALSE)
            {
                $i = 0;
                while (($data = fgetcsv($handle)) !== FALSE && $i != 2)
                {
                    if ($i != 0) {
                        $this->parseRow($data,$classes);
                    }

                    $i++;
                }
                fclose($handle);
            }
        }

        return $this->render('import', [
            'parsed' => $this->parsed,
        ]);
    }

    private function parseRow($data,$classes)
    {

//    getting the classID
        $classNR = trim($data[14]);
        $classLetter = trim($data[15]);
        $clasa = $classNR.''.strtoupper($classLetter);
        $classID = '';
        foreach ($classes as $key => $class){
            if ($clasa == $class){
                $classID = (int)$key;
            }
        }
//    getting classID
//**********************************************************
//        getting birth date

        $name = explode(' ',$data[1]);
        $fname = trim($name[1]);
        $lname = trim($name[0]);
        $email = trim($data[35]);
        $birthDate = $data[8].'-'.$data[9].'-'.$data[10];
        $gender = strtoupper($data[2]);
        $learnLang2 = strtoupper($data[4]);
        $phone = $data[21];
        $address = $data[24].', str. '.$data[25].' '.$data[26].', ap.'.$data[27];

            $model = new Pupil();
            $model->BirthDate = $birthDate;
            $model->Gender = $gender;
            $model->FirstName = $fname;
            $model->LastName = $lname;
            $model->Status = 'Active';
            $model->ClasaID = $classID;
            $model->Email = $email;
            $model->LearnLang2 = $learnLang2;
            $model->Phone = $phone;
            $model->Address = $address;
            $model->event = 'add';

        if($model->save(false)){
            $this->parsed[] = $model->ID;
        }else{

        }
    }
}
