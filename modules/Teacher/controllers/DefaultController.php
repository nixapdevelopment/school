<?php

namespace app\modules\Teacher\controllers;

use Yii;

/**
 * Default controller for the `teacher` module
 */
class DefaultController extends \app\controllers\BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}