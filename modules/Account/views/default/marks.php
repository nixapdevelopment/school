<?php
use app\modules\Mark\models\Mark;
?>
<div class="Account-default-index">
    <h1>Note</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-marks">
            <thead>
            <tr>
                <th>
                   <h5 class="text-center"><?= $lesson->subject->Name ?><br><span>(<?= $teacher->fullName ?>)</span></h5>

                </th>
                <?php foreach ($dates as $timestamp => $date) { ?>
                    <th class="text-center">
                    <span>
                        <?= $date['shortDate'] ?>
                    </span>
                    </th>
                <?php } ?>
                <th>
                    Medie
                </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="padding: 2px 5px; vertical-align: middle;">
                        <div style="width: 140px;">
                            <?= $pupil->fullName ?>

                        </div>
                    </td>
                    <?php foreach ($dates as $timestamp => $date) { ?>
                        <td style="padding: 2px;" class="editable-cell">
                            <input data-pupil-id="<?= $pupil->ID ?>" data-date="<?= $date['systemDate'] ?>" class="mark-input" value="<?= isset($marksGrouped[$pupil->ID][$date['systemDate']]) ? $marksGrouped[$pupil->ID][$date['systemDate']]->Mark : '' ?>" disabled />
                        </td>
                    <?php } ?>
                    <td style="padding: 2px;">
                        <input data-pupil-id="<?= $pupil->ID ?>" value="<?=number_format( Mark::find()->where([
                            'PupilID' => $pupil->ID,
                            'LessonID' => Yii::$app->request->get('lessonID'),
                            'Type' => 'Mark',
                        ])->average('Mark'), 2);?>" class="mark-input average-input" disabled />
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>
