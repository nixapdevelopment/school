<?php

use yii\helpers\Html;
use app\components\CGridView\CGridView;
use yii\widgets\Pjax;
use app\components\CDatepicker\CDatepicker;
use app\modules\Pupil\models\Pupil;
use app\modules\Clasa\models\Clasa;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Pupil\models\PupilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pupils');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pupil-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pupil'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Import'), ['import'], ['class' => 'btn btn-danger']) ?>
    </p>

<?php Pjax::begin(); ?>    
    <?= CGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ID',=
            'FirstName',
            'LastName',
            [
                'attribute' => 'BirthDate',
                'value' => function($model)
                {
                    return $model->NiceDate;
                },
                'filter' => CDatepicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'BirthDate'
                ]),
                'options' => [
                    'style' => 'width:220px;'
                ]
            ],
            [
                'attribute' => 'Gender',
                'value' => function($model)
                {
                    return $model->Gender;
                },
                'filter' => Html::activeDropDownList($searchModel, 'Gender', Pupil::getGenderList(true), ['class' => 'form-control']),
            ],
            [
                'label' => 'Clasa',
                'value' => function($model)
                {
                    return $model->clasa->Number . $model->clasa->Letter;
                },
                'filter' => Html::activeDropDownList($searchModel, 'ClasaID', Clasa::getClassesList(true), ['class' => 'form-control pull-left', 'style' => 'width:50%']),
                'options' => [
                    'style' => 'width: 200px;'
                ],
            ],
            'Phone',
            'Email:email',
            // 'Address',
            // 'Status',

            [
                'class' => 'app\components\CGridView\CActionColumn'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
