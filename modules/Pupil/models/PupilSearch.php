<?php

namespace app\modules\Pupil\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Pupil\models\Pupil;

/**
 * PupilSearch represents the model behind the search form about `app\modules\Pupil\models\Pupil`.
 */
class PupilSearch extends Pupil
{
    
    public $Number = NULL;
    
    public $Letter = NULL;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ClasaID'], 'integer'],
            [['FirstName', 'LastName', 'BirthDate', 'Gender', 'Phone', 'Email', 'Address', 'Status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pupil::find()->joinWith('clasa');

        // add conditions that should always apply here

        $this->load($params);

        // grid filtering conditions
        if ($this->BirthDate)
        {
            $query->andFilterWhere(['BirthDate' => date('c', strtotime($this->BirthDate))]);
        }
        
        if ($this->Gender)
        {
            $query->andFilterWhere(['Gender' => $this->Gender]);
        }
        
        if ($this->Number)
        {
            $query->andFilterWhere(['Number' => $this->Number]);
        }
        
        if ($this->Letter)
        {
            $query->andFilterWhere(['Letter' => $this->Letter]);
        }
        
        if ($this->Phone)
        {
            $query->andFilterWhere(['like', 'Phone', $this->Phone]);
        }
        
        if ($this->Email)
        {
            $query->andFilterWhere(['like', 'Email', $this->Email]);
        }

        if ($this->ClasaID)
        {
            $query->andFilterWhere([
                'ClasaID' => $this->ClasaID,
            ]);
        }

        $query->andFilterWhere(['like', 'FirstName', $this->FirstName])
            ->andFilterWhere(['like', 'LastName', $this->LastName]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
