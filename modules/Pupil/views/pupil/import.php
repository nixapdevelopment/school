<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\file\FileInput;
    use yii\helpers\Url;
    use yii\widgets\Pjax;


$this->title = Yii::t('app', 'Import {modelClass}: ', [
    'modelClass' => 'Pupil',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pupil'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Import');
?>
<div class="pupil-update">

    <h1><?= Html::encode($this->title) ?></h1>
<br/>

<?php $form = ActiveForm::begin([
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]); ?>
    <?php
    if($parsed){
        $pupils = [];
        foreach ($parsed as $id){
            $pupils[] = \app\modules\Pupil\models\Pupil::findOne($id);

        }?>
        <h2>Imported pupils</h2>
        <div class="row">
            <div class="col-md-1">Nr.</div>
            <div class="col-md-1">Nume</div>
            <div class="col-md-1">Prenume</div>
            <div class="col-md-1">Email</div>
            <div class="col-md-1">Clasa</div>
            <div class="col-md-1">Limba Straina</div>
            <div class="col-md-1">Data Nasterii</div>
            <div class="col-md-1">Sex</div>
            <div class="col-md-1">Status</div>
            <div class="col-md-1">Telefon</div>
            <div class="col-md-2">Adresa</div>
        </div><hr>
        <?php
        foreach ($pupils as $key => $pupil){
            ?>

        <div class="row">
            <div class="col-md-1">#<?=$key+1?></div>
            <div class="col-md-1"><?=$pupil->FirstName?></div>
            <div class="col-md-1"><?=$pupil->LastName?></div>
            <div class="col-md-1"><?=$pupil->Email?></div>
            <div class="col-md-1"><?=$pupil->clasa->fullName?></div>
            <div class="col-md-1"><?=$pupil->LearnLang2?></div>
            <div class="col-md-1"><?=$pupil->BirthDate?></div>
            <div class="col-md-1"><?=$pupil->Gender?></div>
            <div class="col-md-1"><?=$pupil->Status?></div>
            <div class="col-md-1"><?=$pupil->Phone?></div>
            <div class="col-md-2"><?=$pupil->Address?></div>
        </div><hr>
    <?php

        }
    }
    ?>
    <br/>
    <div class="row">
        <div class="col-md-4">
            <?= FileInput::widget([
                'name' => 'File',
                'attribute' => 'File',
                'options' => ['multiple' => false],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['csv'],
                    'maxFileCount' => 1,
                    'showRemove' => false,
                    'showUpload' => false,
                ]
            ]); ?>
        </div>
    </div>
    <br />
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Import'), ['class' => 'btn btn-danger']); ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
