<?php

namespace app\modules\Shedule\controllers;

use Yii;
use yii\web\Controller;

/**
 * Default controller for the `shedule` module
 */
class DefaultController extends \app\controllers\BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}