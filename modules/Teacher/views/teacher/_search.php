<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Teacher\models\TeacherSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'FirstName') ?>

    <?= $form->field($model, 'LastName') ?>

    <?= $form->field($model, 'FatherName') ?>

    <?= $form->field($model, 'BirthDate') ?>

    <?php // echo $form->field($model, 'Gender') ?>

    <?php // echo $form->field($model, 'City') ?>

    <?php // echo $form->field($model, 'Street') ?>

    <?php // echo $form->field($model, 'HouseNumber') ?>

    <?php // echo $form->field($model, 'Phone') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'Level') ?>

    <?php // echo $form->field($model, 'IDNP') ?>

    <?php // echo $form->field($model, 'CPAS') ?>

    <?php // echo $form->field($model, 'CNAM') ?>

    <?php // echo $form->field($model, 'Speciality') ?>

    <?php // echo $form->field($model, 'ClasaID') ?>

    <?php // echo $form->field($model, 'RoomID') ?>

    <?php // echo $form->field($model, 'BirthLocation') ?>

    <?php // echo $form->field($model, 'MilitaryStatus') ?>

    <?php // echo $form->field($model, 'RecruitmentDate') ?>

    <?php // echo $form->field($model, 'RecruitmentDocument') ?>

    <?php // echo $form->field($model, 'RecruitmentContract') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
