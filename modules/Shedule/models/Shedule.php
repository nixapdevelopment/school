<?php

namespace app\modules\Shedule\models;

use Yii;
use app\modules\Shedule\models\SheduleItem;

/**
 * This is the model class for table "Shedule".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Days
 * @property string $ActiveDays
 * @property integer $MaxLessons
 * @property string $StartDate
 * @property string $EndDate
 * @property string $Status
 */
class Shedule extends \yii\db\ActiveRecord
{
    
    const StatusActive = 'Active';
    const StatusInactive = 'Inactive';
    
    public static function getStatusList($addEmptyValue = false)
    {
        $result = $addEmptyValue ? ['' => '-'] : [];
        
        $result += [
            self::StatusActive => 'Active',
            self::StatusInactive => 'Inactive',
        ];
        
        return $result;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Shedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Days', 'StartDate', 'EndDate', 'Status'], 'required'],
            [['Name'], 'string', 'max' => 255],
            ['MaxLessons', 'default', 'value' => 7],
            [['Days', 'MaxLessons'], 'integer'],
            [['ActiveDays'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Name' => Yii::t('app', 'Name'),
            'Days' => Yii::t('app', 'Days'),
            'ActiveDays' => Yii::t('app', 'Active Days'),
            'MaxLessons' => Yii::t('app', 'Max Lessons'),
            'StartDate' => Yii::t('app', 'Start Date'),
            'EndDate' => Yii::t('app', 'End Date'),
            'Status' => Yii::t('app', 'Status'),
        ];
    }
    
    public function getActiveDays()
    {
        return empty($this->ActiveDays) ? [] : explode(',', $this->ActiveDays);
    }
    
    public function beforeSave($insert)
    {
        if ($this->Status == self::StatusActive)
        {
            // verify shedule duplications
        }
        
        $this->ActiveDays = empty($this->ActiveDays) ? '' : implode(',', $this->ActiveDays);
        
        $this->StartDate = date('Y-m-d', strtotime($this->StartDate));
        $this->EndDate = date('Y-m-d', strtotime($this->EndDate));
        
        return parent::beforeSave($insert);
    }
    
    public function afterFind()
    {
        $this->StartDate = date('d.m.Y', strtotime($this->StartDate));
        $this->EndDate = date('d.m.Y', strtotime($this->EndDate));
        
        return parent::afterFind();
    }
    
    public function getItems()
    {
        return $this->hasMany(SheduleItem::className(), ['SheduleID' => 'ID']);
    }
    
}
