<?php

namespace app\modules\Shedule\components\SheduleWidget;

use yii\base\Widget;
use yii\helpers\Json;
use yii\web\View;
use app\modules\Clasa\models\Clasa;
use app\modules\Lesson\models\Lesson;
use app\modules\Shedule\components\SheduleWidget\assets\SheduleWidgetAssets;
use app\modules\Shedule\models\SheduleItem;


class SheduleWidget extends Widget
{
    
    public $model = null;
    
    public function init()
    {
        parent::init();
        
        if ($this->model == null)
        {
            throw new Exception("Please set 'model' property");
        }
    }

    
    public function run()
    {
        $classes = Clasa::find()->where(['>','Number',4])->all();
        $lessons = Lesson::find()->addSelect('*')->with('subject')->all();

        $sheduleItemsModels = SheduleItem::find()->with(['lesson', 'lesson.subject'])->where(['SheduleID' => $this->model->ID])->all();
        
        $sheduleItems = [];
        foreach ($sheduleItemsModels as $index => $sim)
        {
            $sheduleItems[$sim->ClasaID][$sim->Day][$sim->Time][$index] = $sim;
        }
        
        $this->getView()->registerJs("var Shedule = " . Json::encode($this->model), View::POS_HEAD);
        $this->getView()->registerAssetBundle(SheduleWidgetAssets::className(), View::POS_BEGIN);
        
        return $this->render('index', [
            'model' => $this->model,
            'classes' => $classes,
            'lessons' => $lessons,
            'sheduleItems' => $sheduleItems,
        ]);
    }
    
}