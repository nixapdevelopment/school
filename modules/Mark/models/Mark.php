<?php

namespace app\modules\Mark\models;

use Yii;
use app\modules\Lesson\models\Lesson;
use app\modules\Pupil\models\Pupil;
use app\modules\Teacher\models\Teacher;

/**
 * This is the model class for table "Mark".
 *
 * @property integer $ID
 * @property integer $PupilID
 * @property integer $LessonID
 * @property integer $TeacherID
 * @property string $Date
 * @property integer $Mark
 * @property string $Type
 *
 * @property Lesson $lesson
 * @property Pupil $pupil
 * @property Teacher $teacher
 */
class Mark extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Mark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['PupilID', 'Date', 'Type'], 'required'],
            [['PupilID', 'LessonID', 'TeacherID', 'Mark'], 'integer'],
            [['Date'], 'safe'],
            [['Type'], 'string', 'max' => 20],
            [['LessonID'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['LessonID' => 'ID']],
            [['PupilID'], 'exist', 'skipOnError' => true, 'targetClass' => Pupil::className(), 'targetAttribute' => ['PupilID' => 'ID']],
            [['TeacherID'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['TeacherID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'PupilID' => Yii::t('app', 'Pupil ID'),
            'LessonID' => Yii::t('app', 'Lesson ID'),
            'TeacherID' => Yii::t('app', 'Teacher ID'),
            'Date' => Yii::t('app', 'Date'),
            'Mark' => Yii::t('app', 'Mark'),
            'Type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['ID' => 'LessonID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPupil()
    {
        return $this->hasOne(Pupil::className(), ['ID' => 'PupilID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['ID' => 'TeacherID']);
    }
}
