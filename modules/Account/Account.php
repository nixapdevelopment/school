<?php

namespace app\modules\Account;

use app\components\CModule;
/**
 * Account module definition class
 */
class Account extends CModule
{

    public $controllerNamespace = 'app\modules\Account\controllers';

    public function init()
    {
        parent::init();

        $this->layoutPath = \Yii::getAlias('@app/views/themes/school/layouts/account');
    }
}
