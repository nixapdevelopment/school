<?php

use yii\helpers\Html;
use app\components\CGridView\CGridView;
use app\modules\Lesson\models\Lesson;
use app\modules\Subject\models\Subject;
use yii\widgets\Pjax;
use app\modules\Clasa\models\Clasa;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Lesson\models\LessonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lessons');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="lesson-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Lesson'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="row">
        <div class="col-md-6">
            <?php Pjax::begin(); ?>    
                <?= CGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'subject.Name',
                            'filter' => Html::activeDropDownList($searchModel, 'SubjectID', Subject::getList(true), ['class' => 'form-control']),
                        ],
                        [
                            'attribute' => 'lessonClasas.ClasaID',
                            'filter' => Html::activeDropDownList($searchModel, 'ClasaID', Clasa::getClassesList(true), ['class' => 'form-control']),
                            'value' => function($model)
                            {
                                return implode(', ', ArrayHelper::getColumn($model->lessonClasas, 'clasa.fullName'));
                            }
                        ],
                        [
                            'attribute' => 'Type',
                            //'filter' => Html::activeDropDownList($searchModel, 'Type', Lesson::getTypesList(true), ['class' => 'form-control']),
                            'filter' => false,
                            'value' => function($model)
                            {
                                return Lesson::getTypeName($model->Type);
                            }
                        ],

                        [
                            'class' => 'app\components\CGridView\CActionColumn'
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
