<?php

namespace app\components\Enums;


class MilitaryStatus
{
    
    const Yes = 'Yes';
    const No = 'No';

    public static function getList($addEmptyValue = false)
    {
        $result = $addEmptyValue ? ['' => '-'] : [];
        
        $result += [
            self::Yes => 'Da',
            self::No => 'Nu',
        ];
        
        return $result;
    }
    
}
