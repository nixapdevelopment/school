<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\CYearSelect\CYearSelect;
use app\modules\Clasa\models\Clasa;

/* @var $this yii\web\View */
/* @var $model app\modules\Clasa\models\Clasa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clasa-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'StartYear')->widget(CYearSelect::className(), [
                'yearStart' => 2000,
                'yearStartType' => 'fix',
                'yearEnd' => date('Y') + 1,
                'yearEndType' => 'fix',
                'addEmptyValue' => true,
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'EndYear')->widget(CYearSelect::className(), [
                'yearStart' => 2000,
                'yearStartType' => 'fix',
                'yearEnd' => date('Y') + 15,
                'yearEndType' => 'fix',
                'addEmptyValue' => true,
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <?= $form->field($model, 'Number')->dropDownList(Clasa::getClassNumbers(true)) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'Letter')->dropDownList(Clasa::getClassLetters(true)) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Type')->dropDownList(Clasa ::getTypesList()) ?>
        </div>
    </div>

    <?= $form->field($model, 'Description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
