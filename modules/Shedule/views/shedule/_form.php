<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\CDatepicker\CDatepicker;
use app\components\CSelect2\CSelect2;
use app\modules\Shedule\models\Shedule;

/* @var $this yii\web\View */
/* @var $model app\modules\Shedule\models\Shedule */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shedule-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Days')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'MaxLessons')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'StartDate')->widget(CDatepicker::className()) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'EndDate')->widget(CDatepicker::className()) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Status')->widget(CSelect2::className(), [
                'data' => Shedule::getStatusList(true)
            ]) ?>
        </div>
        <?php if (!$model->isNewRecord) { ?>
        <div class="col-md-5">
            <?= $form->field($model, 'ActiveDays')->checkboxList($allActiveDays, [
                'value' => $model->activeDays
            ]) ?>
        </div>
        <?php } ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
