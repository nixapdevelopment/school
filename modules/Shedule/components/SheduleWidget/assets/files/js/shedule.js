function initSheduleJs()
{
    $('.shedule-widget *[data-entity="lesson"]').draggable({
        revert: 'invalid',
        stack: false,
        start: function(event, ui)
        {
            var draggable = ui.helper;
            var clasaIDs = draggable.attr('data-clasa-id').split(',');
            $.each(clasaIDs, function(key, clasaID) {
                $('.droppable[data-clasa-id=' + clasaID + ']').addClass('placeable');
            });
        },
        stop: function(event, ui)
        {
            var draggable = ui.helper;
            var clasaIDs = draggable.attr('data-clasa-id').split(',');
            $.each(clasaIDs, function(key, clasaID) {
                $('.droppable[data-clasa-id=' + clasaID + ']').removeClass('placeable');
            });
        }
    });
    
    $('.droppable').droppable({
        tolerance: 'intersect',
        accept: '*[data-entity="lesson"]',
        classes: {
            "ui-droppable-active": "ui-state-active",
            "ui-droppable-hover": "ui-state-hover"
        },
        drop: function(event, ui) {
            $(ui.draggable).detach().css({top: 0,left: 0}).appendTo(this);
            var cell = $(this);
            var item = ui.draggable;
            
            if (cell.hasClass('free-lessons'))
            {
                // remove shedule item
                if (item.attr('data-id'))
                {
                    $.post(
                        siteUrl('admin/shedule/ajax/delete'), 
                        {ID: item.attr('data-id')}, 
                        function(){
                            $.pjax.reload({container: "#shedule-widget-pjax", timeout: 10000});
                        }
                    );
                }
            }
            else
            {
                if (item.attr('data-id'))
                {
                    // update shedule item
                    $.post(
                        siteUrl('admin/shedule/ajax/update'), 
                        {ID: item.attr('data-id'), SheduleID: Shedule.ID, LessonID: item.attr('data-lesson-id'), ClasaID: cell.attr('data-clasa-id'), Day: cell.attr('data-day'), Time: cell.attr('data-time')}, 
                        function(json){
                            $.pjax.reload({container: "#shedule-widget-pjax", timeout: 10000});
                        }
                    );
                }
                else
                {
                    // insert shedule item
                    $.post(
                        siteUrl('admin/shedule/ajax/create'), 
                        {SheduleID: Shedule.ID, LessonID: item.attr('data-lesson-id'), ClasaID: item.attr('data-clasa-id'), Day: cell.attr('data-day'), Time: cell.attr('data-time')}, 
                        function(json){
                            $.pjax.reload({container: "#shedule-widget-pjax", timeout: 10000});
                        }
                    );
                }
            }
        }
    });
    
    $('[data-toggle="tooltip"]').tooltip();
}