<?php

namespace app\components\CGridView;

use kartik\grid\ActionColumn;


class CActionColumn extends ActionColumn
{
    
    public $width = '100px;';
    
    public $template = '<div class="text-center grid-buttons">{update}</b>&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>';
    
}
