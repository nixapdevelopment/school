<?php

namespace app\modules\Subject\models;

use Yii;

/**
 * This is the model class for table "Subject".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $ShortName
 * @property string $Color
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'required'],
            [['Name'], 'string', 'max' => 255],
            [['Color'], 'string', 'max' => 10],
            [['ShortName'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Name' => Yii::t('app', 'Name'),
            'ShortName' => Yii::t('app', 'Short Name'),
            'Color' => Yii::t('app', 'Color'),
        ];
    }
    
    public static function getList($addEmptyValue = false,$cid = false)
    {

            $result = $addEmptyValue ? ['' => '-'] : [];
            $models = Subject::find()->all();
            foreach ($models as $model) {
                $result[$model->ID] = $model->Name;
            }

        return $result;
    }
    
}
