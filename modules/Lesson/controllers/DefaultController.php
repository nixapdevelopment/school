<?php

namespace app\modules\Lesson\controllers;

use Yii;

/**
 * Default controller for the `lesson` module
 */
class DefaultController extends \app\controllers\BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}