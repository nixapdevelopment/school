<?php

namespace app\modules\Schoolroom;

/**
 * schoolroom module definition class
 */
class Schoolroom extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Schoolroom\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
