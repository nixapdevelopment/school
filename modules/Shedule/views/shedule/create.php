<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Shedule\models\Shedule */

$this->title = Yii::t('app', 'Create Shedule');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shedules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shedule-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
