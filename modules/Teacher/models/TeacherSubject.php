<?php

namespace app\modules\Teacher\models;

use Yii;
use app\modules\Subject\models\Subject;
use app\modules\Teacher\models\Teacher;

/**
 * This is the model class for table "TeacherSubject".
 *
 * @property integer $ID
 * @property integer $TeacherID
 * @property integer $SubjectID
 *
 * @property Subject $subject
 * @property Teacher $teacher
 */
class TeacherSubject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TeacherSubject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TeacherID', 'SubjectID'], 'required'],
            [['TeacherID', 'SubjectID'], 'integer'],
            [['SubjectID'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['SubjectID' => 'ID']],
            [['TeacherID'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['TeacherID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TeacherID' => Yii::t('app', 'Teacher ID'),
            'SubjectID' => Yii::t('app', 'Subject ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['ID' => 'SubjectID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['ID' => 'TeacherID']);
    }
}
