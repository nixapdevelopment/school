<?php

use yii\helpers\Html;
use app\components\CGridView\CGridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Schoolroom\models\SchoolroomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Schoolrooms');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schoolroom-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Schoolroom'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="row">
        <div class="col-md-6">
            <?php Pjax::begin(); ?>    
                <?= CGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'ID',
                        'Number',
                        'Capacity',
                        'Name',

                        [
                            'class' => 'app\components\CGridView\CActionColumn'
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
