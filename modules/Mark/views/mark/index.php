<?php

    use app\components\CGridView\CGridView;
    use app\modules\Subject\models\Subject;
    use yii\helpers\Html;
    use yii\widgets\Pjax;
    use app\modules\Clasa\models\Clasa;
    use yii\helpers\Url;

?>

<div class="mark-default-index">
    <h1></h1>
    <div class="row">
        <div class="col-md-5">
            <?php Pjax::begin() ?>
                <?= CGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                            'options' => [
                                'width' => '50px'
                            ]
                        ],

                        [
                            'attribute' => 'SubjectID',
                            'filter' => Html::activeDropDownList($searchModel, 'SubjectID', Subject::getList(true), ['class' => 'form-control']),
                            'value' => function($model)
                            {
                                return $model->Subject;
                            }
                        ],
                        [
                            'attribute' => 'ClasaID',
                            'filter' => Html::activeDropDownList($searchModel, 'ClasaID', Clasa::getClassesList(true), ['class' => 'form-control']),
                            'value' => function($model)
                            {
                                return $model->Clasa;
                            }
                        ],

                        [
                            'class' => 'app\components\CGridView\CActionColumn',
                            'template' => '{update}',
                            'buttons' => [
                                'update' => function ($url, $model)
                                {
                                    return Html::a(Html::tag('i', '', ['class' => 'fa fa-edit']), Url::to(['view', 'lessonID' => $model->ID, 'clasaID' => $model->ClasaID]), ['data-pjax' => 0]);
                                }
                            ]
                        ],
                    ],
                ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>
    
</div>
