<?php

use yii\helpers\Html;
use app\modules\Shedule\components\SheduleWidget\SheduleWidget;

/* @var $this yii\web\View */
/* @var $model app\modules\Shedule\models\Shedule */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Shedule',
]) . $model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shedules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="shedule-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
//        'checkedActiveDays' => $checkedActiveDays,
        'allActiveDays' => $allActiveDays,
    ]) ?>
    
    <div>
        <?= SheduleWidget::widget([
            'model' => $model
        ]) ?>
    </div>

</div>
