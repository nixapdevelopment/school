<?php

namespace app\modules\Clasa\controllers;

use Yii;

/**
 * Default controller for the `clasa` module
 */
class DefaultController extends \app\controllers\BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}