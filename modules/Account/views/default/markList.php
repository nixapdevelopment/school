<?php
use \yii\widgets\Pjax;
use \app\components\CGridView\CGridView;
use yii\helpers\Html;
use app\modules\Subject\models\Subject;
use app\modules\Clasa\models\Clasa;
use yii\helpers\Url;
?>

<div class="mark-default-index">
    <h1><?=$pupil->fullName?></h1>
    <br><br>
    <h3>Subiecte</h3>
    <div class="row">
        <div class="col-md-5">
            <?php Pjax::begin() ?>
            <?= CGridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'options' => [
                            'width' => '50px'
                        ]
                    ],
                    [
                        'attribute' => 'SubjectID',
                        'filter' => Html::activeDropDownList($searchModel, 'SubjectID', Subject::getList(true), ['class' => 'form-control']),
                        'value' => function($model){
                            return $model->Subject;
                        }
                    ],
                    [
                        'attribute' => 'ClasaID',
                        'filter' => false,
                        'value' => function($model){
                            return $model->Clasa;
                        }
                    ],
                    [
                        'class' => 'app\components\CGridView\CActionColumn',
                        'template' => '{update}',
                        'buttons' => [
                            'update' => function ($url, $model)
                            {
                                return Html::a(Html::tag('i', '', ['class' => 'fa fa-edit']), Url::to(['marks', 'lessonID' => $model->ID]), ['data-pjax' => 0]);
                            }
                        ]
                    ],
                ],
            ]); ?>
            <?php Pjax::end() ?>
        </div>
    </div>

</div>