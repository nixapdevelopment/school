<?php
use yii\widgets\DetailView;
?>
<div class="account-default-index">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <?php
            if ($account) {
                ?>
                <?= DetailView::widget([
                    'model' => $account,
                    'attributes' => [
                        [
                            'attribute' => 'pupil.fullName',
                            'label' => 'Elev'
                        ],
                        [
                            'attribute' => 'pupil.clasa.fullName',
                            'label' => 'Clasa'
                        ],
                        [
                            'attribute' => 'pupil.Address',
                            'label' => 'Adresa'
                        ],
                        [
                            'attribute' => 'pupil.BirthDate',
                            'label' => 'Data Nasterii'
                        ],
                        [
                            'attribute' => 'user.Email',
                            'label' => 'Email/Login'
                        ],
                        [
                            'attribute' => 'user.AuthKey',
                            'label' => 'Parola'
                        ],
                    ],
                ]) ?>
                <?php
            }else{
                ?><br> <br>
                <h1>Contul Dvs. nu este activat!</h1>
            <?php
            }
            ?>
        </div>
    </div>
</div>
