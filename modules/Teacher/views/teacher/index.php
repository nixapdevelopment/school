<?php

use yii\helpers\Html;
use app\components\CGridView\CGridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\modules\Subject\models\Subject;
use app\components\Enums\Gender;
use app\modules\Clasa\models\Clasa;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Teacher\models\TeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Teachers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teacher-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Teacher'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    
    <?= CGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ID',
            'FirstName',
            'LastName',
            [
                'attribute' => 'SubjectID',
                'value' => function($model)
                {
                    return implode(', ', ArrayHelper::map($model->teacherSubjects, 'SubjectID', 'subject.Name'));
                },
                'filter' => Html::activeDropDownList($searchModel, 'SubjectID', Subject::getList('-'), ['class' => 'form-control']),
            ],
            //'FatherName',
            //'BirthDate',
            [
                'attribute' => 'Gender',
                'filter' => Html::activeDropDownList($searchModel, 'Gender', Gender::getList(true), ['class' => 'form-control'])
            ],
            // 'City',
            // 'Street',
            // 'HouseNumber',
            // 'Phone',
            // 'Email:email',
            // 'Level',
            // 'IDNP',
            // 'CPAS',
            // 'CNAM',
            // 'Speciality',
            [
                'attribute' => 'ClasaID',
                'filter' => Html::activeDropDownList($searchModel, 'ClasaID', Clasa::getClassesList(true), ['class' => 'form-control']),
                'value' => function($model)
                {
                    if ($model->clasa) {
                        return $model->clasa->FullName;
                    }
                }
            ],
            [
                'attribute' => 'RoomID',
                'filter' => ''
            ],
            // 'BirthLocation',
            // 'MilitaryStatus',
            // 'RecruitmentDate',
            // 'RecruitmentDocument',
            // 'RecruitmentContract',

            ['class' => 'app\components\CGridView\CActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
