<?php

namespace app\controllers;

use app\controllers\CController;

class SiteController extends CController
{
    
    public $layout = 'frontend';

    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['/user/login']);
    }
    
}