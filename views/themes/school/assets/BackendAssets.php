<?php

namespace app\views\themes\school\assets;

use yii\web\AssetBundle;


class BackendAssets extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/school/assets/files';
    
    public $css = [
        'css/backend.css'
    ];
    
    public $js = [
        
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'yiister\gentelella\assets\Asset'
    ];
    
}