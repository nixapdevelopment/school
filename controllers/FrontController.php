<?php

namespace app\controllers;

use Yii;
use app\controllers\CController;

class FrontController extends CController
{

    public function init()
    {
        parent::init();

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'backend';
        
        Yii::$container->set('yii\widgets\Pjax', ['timeout' => 15000]);
    }

}