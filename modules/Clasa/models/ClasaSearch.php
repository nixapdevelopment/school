<?php

namespace app\modules\Clasa\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Clasa\models\Clasa;

/**
 * ClasaSearch represents the model behind the search form about `app\modules\Clasa\models\Clasa`.
 */
class ClasaSearch extends Clasa
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'SchoolID', 'StartYear', 'EndYear', 'Number'], 'integer'],
            [['Letter', 'Description', 'Type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clasa::find();

        // add conditions that should always apply here
        $query->andFilterWhere(['Status' => Clasa::StatusActive]);
        $query->addOrderBy('Number ASC');
        $query->addOrderBy('Letter ASC');

        $this->load($params);

        if ($this->Number != '')
        {
            $query->andFilterWhere(['Number' => $this->Number]);
        }

        if ($this->StartYear)
        {
            $query->andFilterWhere(['StartYear' => $this->StartYear]);
        }

        if ($this->EndYear)
        {
            $query->andFilterWhere(['EndYear' => $this->EndYear]);
        }

        if ($this->Type)
        {
            $query->andFilterWhere(['Type' => $this->Type]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
