<?php

namespace app\modules\Mark\controllers;

use Yii;
use app\modules\Mark\models\MarkListSearch;
use app\modules\Shedule\models\Shedule;
use app\modules\Shedule\models\SheduleItem;
use app\modules\Pupil\models\Pupil;
use app\modules\Mark\models\Mark;

/**
 * Default controller for the `mark` module
 */
class MarkController extends DefaultController
{
    
    public function actionIndex()
    {
        $searchModel = new MarkListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
    
    public function actionView($lessonID, $clasaID)
    {
        $shedule = Shedule::find()->where(['Status' => Shedule::StatusActive])->one();
        $sheduleItems = SheduleItem::find()->where(['SheduleID' => $shedule->ID, 'LessonID' => $lessonID, 'ClasaID' => $clasaID])->indexBy('Day')->all();
        $pupils = Pupil::find()->where(['ClasaID' => $clasaID])->all();
        $marks = Mark::find()->where(['LessonID' => $lessonID])->all();
        
        $marksGrouped = [];
        foreach ($marks as $mark)
        {
            $marksGrouped[$mark->PupilID][$mark->Date] = $mark;
        }
        
        $maxDate = strtotime($shedule->EndDate);
        
        if ($maxDate > strtotime('+ 1 month'))
        {
            $maxDate = strtotime('+ 1 month');
        }

        $dates = [];
        for ($date = strtotime($shedule->StartDate); $date <= $maxDate; $date += 86400)
        {
            $day = date('w', $date);
            if (isset($sheduleItems[$day]))
            {
                $dates[$date] = [
                    'systemDate' => date('Y-m-d', $date),
                    'fullDate' => date('d.m.Y', $date),
                    'shortDate' => date('d.m', $date),
                ];
            }
        }
        return $this->render('view', [
            'shedule' => $shedule,
            'sheduleItems' => $sheduleItems,
            'dates' => $dates,
            'pupils' => $pupils,
            'marksGrouped' => $marksGrouped,
        ]);
    }
    
    public function actionSave()
    {
        $mark = trim(Yii::$app->request->post('Mark'));

        Mark::deleteAll([
            'PupilID' => Yii::$app->request->post('PupilID'),
            'LessonID' => Yii::$app->request->post('LessonID'),
            'Date' => Yii::$app->request->post('Date'),
        ]);

        if (!empty($mark)){

            if (is_numeric($mark))
            {
                if ($mark <= 10 && $mark > 0) {
                    $mark = (int)$mark;
                    $type = 'Mark';
                }elseif($mark<1){
                    $mark = 1;
                    $type = 'Mark';
                }else{
                    $mark = 10;
                    $type = 'Mark';
                }
            }
            else
            {
                if (strtolower($mark) == 'a')
                {
                    // m and b
                    $mark = 'a';
                    $type = 'Skip';
                }
                else
                {
                    $mark = 'a';
                    $type = 'Skip';
                }
            }

            $data = [
                'PupilID' => Yii::$app->request->post('PupilID'),
                'LessonID' => Yii::$app->request->post('LessonID'),
                'TeacherID' => 1,
                'Date' => Yii::$app->request->post('Date'),
                'Mark' => $mark,
                'Type' => $type,
            ];

            $markModel = new Mark($data);
            $markModel->save(false);

            $average = Mark::find()->where([
                'PupilID' => Yii::$app->request->post('PupilID'),
                'LessonID' => Yii::$app->request->post('LessonID'),
                'Type' => 'Mark',
            ])->average('Mark');

            exit(json_encode([
                'error' => false,
                'average' => number_format($average, 2),
                'model' => $data,
            ]));
        }else{
            $average = Mark::find()->where([
                'PupilID' => Yii::$app->request->post('PupilID'),
                'LessonID' => Yii::$app->request->post('LessonID'),
                'Type' => 'Mark',
            ])->average('Mark');

            $data = [
                'PupilID' => Yii::$app->request->post('PupilID'),
                'LessonID' => Yii::$app->request->post('LessonID'),
            ];

            exit(json_encode([
                'error' => false,
                'average' => number_format($average, 2),
                'model' =>$data
            ]));
        }

    }

}