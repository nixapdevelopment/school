<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\components\CDatepicker\CDatepicker;
use app\modules\Schoolroom\models\Schoolroom;
use app\components\Enums\Gender;
use app\modules\Clasa\models\Clasa;
use app\components\CSelect2\CSelect2Multiple;
use app\modules\Subject\models\Subject;
use app\components\CSelect2\CSelect2;

/* @var $this yii\web\View */
/* @var $model app\modules\Teacher\models\Teacher */
/* @var $form yii\widgets\ActiveForm */


?>
<div class="teacher-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'FirstName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'LastName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'FatherName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'BirthDate')->widget(CDatepicker::className(), [
                'options' => [
                    'value' => empty($model->BirthDate) ? date('d.m.Y') : date('d.m.Y', strtotime($model->BirthDate))
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Gender')->widget(CSelect2::className(), [
                'data' => Gender::getList(true)
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'ClasaID')->widget(CSelect2::className(), [
                'data' => Clasa::getClassesList(true)
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'RoomID')->widget(CSelect2::className(), [
                'data' => Schoolroom::getList('Selectati')
            ]) ?>
        </div>
        <div class="col-md-6">
            <label class="control-label">Subjects</label>
            <?= CSelect2Multiple::widget([
                'name' => $model->formName() . '[SubjectIDs][]',
                'data' => Subject::getList(),
                'value' => $model->teacherSubjectIDs,
            ]) ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'City')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Street')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'HouseNumber')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'BirthLocation')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'Phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'Speciality')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Level')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'RecruitmentDate')->widget(CDatepicker::className(), [
                'options' => [
                    'value' => empty($model->RecruitmentDate) ? date('d.m.Y') : date('d.m.Y', strtotime($model->RecruitmentDate))
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'RecruitmentDocument')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'RecruitmentContract')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'IDNP')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'CPAS')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'CNAM')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'MilitaryStatus')->dropDownList(app\components\Enums\MilitaryStatus::getList(true)) ?>
        </div>
    </div>
    
    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
