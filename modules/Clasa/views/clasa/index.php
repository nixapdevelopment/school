<?php

use yii\helpers\Html;
use app\components\CGridView\CGridView;
use yii\widgets\Pjax;
use app\modules\Clasa\models\Clasa;
use app\components\CYearSelect\CYearSelect;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Clasa\models\ClasaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clase');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clasa-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> Adauga Clasa'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="row">
        <div class="col-md-7">
            <?php Pjax::begin(); ?>    
                <?= CGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'class' => 'yii\grid\SerialColumn',
                        ],

                        [
                            'label' => 'Nume',
                            'value' => function($model)
                            {
                                return $model->Number . $model->Letter;
                            },
                            'filter' => Html::activeDropDownList($searchModel, 'Number', Clasa::getClassNumbers(true), ['class' => 'form-control'])
                        ],
                        [
                            'attribute' => 'StartYear',
                            'filter' => CYearSelect::widget([
                                'model' => $searchModel,
                                'attribute' => 'StartYear',
                                'yearStart' => 2000,
                                'yearStartType' => 'fix',
                                'yearEnd' => date('Y') + 1,
                                'yearEndType' => 'fix',
                                'addEmptyValue' => true,
                            ])
                        ],
                        [
                            'attribute' => 'EndYear',
                            'filter' => CYearSelect::widget([
                                'model' => $searchModel,
                                'attribute' => 'EndYear',
                                'yearStart' => 2000,
                                'yearStartType' => 'fix',
                                'yearEnd' => date('Y') + 15,
                                'yearEndType' => 'fix',
                                'addEmptyValue' => true,
                            ])
                        ],
                        [
                            'attribute' => 'Type',
                            'filter' => Html::activeDropDownList($searchModel, 'Type', Clasa ::getTypesList(), ['class' => 'form-control'])
                        ],

                        [
                            'class' => 'app\components\CGridView\CActionColumn'
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
