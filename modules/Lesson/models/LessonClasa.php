<?php

namespace app\modules\Lesson\models;

use Yii;
use app\modules\Lesson\models\Lesson;
use app\modules\Clasa\models\Clasa;

/**
 * This is the model class for table "LessonClasa".
 *
 * @property integer $ID
 * @property integer $LessonID
 * @property integer $ClasaID
 *
 * @property Lesson $lesson
 * @property Clasa $clasa
 */
class LessonClasa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LessonClasa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LessonID'], 'required'],
            [['LessonID', 'ClasaID'], 'integer'],
            [['LessonID'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['LessonID' => 'ID']],
            [['ClasaID'], 'exist', 'skipOnError' => true, 'targetClass' => Clasa::className(), 'targetAttribute' => ['ClasaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'LessonID' => Yii::t('app', 'Lesson ID'),
            'ClasaID' => Yii::t('app', 'Clasa ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['ID' => 'LessonID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasa()
    {
        return $this->hasOne(Clasa::className(), ['ID' => 'ClasaID']);
    }
}
