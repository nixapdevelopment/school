<?php

namespace app\modules\Teacher\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\Clasa\models\Clasa;
use app\modules\Teacher\models\TeacherSubject;

/**
 * This is the model class for table "Teacher".
 *
 * @property integer $ID
 * @property string $FirstName
 * @property string $LastName
 * @property string $FatherName
 * @property string $BirthDate
 * @property string $Gender
 * @property string $City
 * @property string $Street
 * @property string $HouseNumber
 * @property string $Phone
 * @property string $Email
 * @property string $Level
 * @property string $IDNP
 * @property string $CPAS
 * @property string $CNAM
 * @property string $Speciality
 * @property integer $ClasaID
 * @property integer $RoomID
 * @property string $BirthLocation
 * @property string $MilitaryStatus
 * @property string $RecruitmentDate
 * @property string $RecruitmentDocument
 * @property string $RecruitmentContract
 *
 * @property Room $room
 * @property Clasa $clasa
 * @property TeacherSubject[] $teacherSubjects
 */
class Teacher extends \yii\db\ActiveRecord
{
    
    public $SubjectIDs = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['RoomID', 'default', 'value' => NULL],
            ['ClasaID', 'default', 'value' => NULL],
            [['FirstName', 'LastName','FatherName','Gender'], 'required'],
            [['BirthDate', 'RecruitmentDate', 'SubjectIDs'], 'safe'],
            [['ClasaID', 'RoomID'], 'integer'],
            [['FirstName', 'LastName', 'FatherName', 'City', 'Street', 'HouseNumber', 'Phone', 'Email', 'Level', 'IDNP', 'CPAS', 'CNAM', 'Speciality', 'RecruitmentDocument', 'RecruitmentContract'], 'string', 'max' => 255],
            [['Gender', 'MilitaryStatus'], 'string', 'max' => 20],
            [['BirthLocation'], 'string', 'max' => 500],
            //[['RoomID'], 'exist', 'skipOnError' => true, 'targetClass' => Room::className(), 'targetAttribute' => ['RoomID' => 'ID']],
            [['ClasaID'], 'exist', 'skipOnError' => true, 'targetClass' => Clasa::className(), 'targetAttribute' => ['ClasaID' => 'ID']],
        ];
    }
    
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        
        $this->BirthDate = empty($this->BirthDate) ? NULL : date('c', strtotime($this->BirthDate));
        $this->RecruitmentDate = empty($this->RecruitmentDate) ? NULL : date('c', strtotime($this->RecruitmentDate));
        
        return true;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        // save teacher subjects
        TeacherSubject::deleteAll(['TeacherID' => $this->ID]);
        foreach ($this->SubjectIDs as $subjectID)
        {
            $model = new TeacherSubject([
                'TeacherID' => $this->ID,
                'SubjectID' => $subjectID,
            ]);
            $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'FirstName' => Yii::t('app', 'First Name'),
            'LastName' => Yii::t('app', 'Last Name'),
            'FatherName' => Yii::t('app', 'Father Name'),
            'BirthDate' => Yii::t('app', 'Birth Date'),
            'Gender' => Yii::t('app', 'Gender'),
            'City' => Yii::t('app', 'City'),
            'Street' => Yii::t('app', 'Street'),
            'HouseNumber' => Yii::t('app', 'House Number'),
            'Phone' => Yii::t('app', 'Phone'),
            'Email' => Yii::t('app', 'Email'),
            'Level' => Yii::t('app', 'Level'),
            'IDNP' => Yii::t('app', 'Idnp'),
            'CPAS' => Yii::t('app', 'Cpas'),
            'CNAM' => Yii::t('app', 'Cnam'),
            'Speciality' => Yii::t('app', 'Speciality'),
            'ClasaID' => Yii::t('app', 'Clasa ID'),
            'RoomID' => Yii::t('app', 'Room ID'),
            'BirthLocation' => Yii::t('app', 'Birth Location'),
            'MilitaryStatus' => Yii::t('app', 'Military Status'),
            'RecruitmentDate' => Yii::t('app', 'Recruitment Date'),
            'RecruitmentDocument' => Yii::t('app', 'Recruitment Document'),
            'RecruitmentContract' => Yii::t('app', 'Recruitment Contract'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        //return $this->hasOne(Room::className(), ['ID' => 'RoomID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasa()
    {
        return $this->hasOne(Clasa::className(), ['ID' => 'ClasaID']);
    }
    
    public static function getList($addEmptyValue = false)
    {
        $result = $addEmptyValue ? ['' => '-'] : [];
        
        $models = Teacher::find()->all();
        
        foreach ($models as $model)
        {
            $result[$model->ID] = $model->FullName;
        }
        
        return $result;
    }
    
    public function getFullName()
    {
        return $this->FirstName . ' ' . $this->LastName;
    }
    
    public function getTeacherSubjects()
    {
        return $this->hasMany(TeacherSubject::className(), ['TeacherID' => 'ID']);
    }
    
    public function getTeacherSubjectIDs()
    {
        return ArrayHelper::map($this->teacherSubjects, 'SubjectID', 'SubjectID');
    }
    
}