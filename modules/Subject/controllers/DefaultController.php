<?php

namespace app\modules\Subject\controllers;

use Yii;

/**
 * Default controller for the `subject` module
 */
class DefaultController extends \app\controllers\BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}