<?php

namespace app\modules\Mark\controllers;

use Yii;

/**
 * Default controller for the `mark` module
 */
class DefaultController extends \app\controllers\BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}