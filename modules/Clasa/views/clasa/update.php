<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Clasa\models\Clasa */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Clasa',
]) . $model->Number . $model->Letter;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clase'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Number . $model->Letter, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="clasa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
