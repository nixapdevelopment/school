<?php

namespace app\components;

use yii\base\Module;


class CModule extends Module
{
    
    public $name = '';
    
    public function init()
    {
        parent::init();
        
        $this->name = $this->getUniqueId();
    }
    
}