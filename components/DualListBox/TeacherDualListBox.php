<?php

namespace app\components\DualListBox;


class TeacherDualListBox extends DualListBox
{
    
    public $data_id = 'ID';
    public $data_value = 'fullName';
    
    public $lngOptions = [
        'search_placeholder' => 'Cautare',
        'showing' => '',
        'available' => 'Toate',
        'selected' => 'Selectate',
    ];
    
    
    public function init()
    {
        parent::init();
        
        $this->data = \app\modules\Teacher\models\Teacher::find();
    }
    
}