<?php

namespace app\modules\User\controllers;

use Yii;
use app\models\User\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\User\LoginForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class LoginController extends Controller
{
	
	public function init()
	{
		parent::init();
		
		$this->layout = 'login';
	}
    
    public function actionIndex()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            if (Yii::$app->user->identity->Type == User::TypeAdmin)
            {
                return $this->redirect(['/admin']);
            }else {

                return $this->redirect(['/account']);
            }
        }
        
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
