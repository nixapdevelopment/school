<?php

namespace app\modules\Lesson\models;

use Yii;
use app\modules\Lesson\models\Lesson;
use app\modules\Schoolroom\models\Schoolroom;

/**
 * This is the model class for table "LessonSchoolroom".
 *
 * @property integer $ID
 * @property integer $LessonID
 * @property integer $SchoolroomID
 *
 * @property Lesson $lesson
 * @property Schoolroom $schoolroom
 */
class LessonSchoolroom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LessonSchoolroom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LessonID'], 'required'],
            [['LessonID', 'SchoolroomID'], 'integer'],
            [['LessonID'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['LessonID' => 'ID']],
            [['SchoolroomID'], 'exist', 'skipOnError' => true, 'targetClass' => Schoolroom::className(), 'targetAttribute' => ['SchoolroomID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'LessonID' => Yii::t('app', 'Lesson ID'),
            'SchoolroomID' => Yii::t('app', 'Schoolroom ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['ID' => 'LessonID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolroom()
    {
        return $this->hasOne(Schoolroom::className(), ['ID' => 'SchoolroomID']);
    }
}
