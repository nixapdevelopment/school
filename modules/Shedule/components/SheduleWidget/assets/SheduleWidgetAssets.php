<?php

namespace app\modules\Shedule\components\SheduleWidget\assets;

use yii\web\AssetBundle;


class SheduleWidgetAssets extends AssetBundle
{
    
    public $sourcePath = '@app/modules/Shedule/components/SheduleWidget/assets/files';
    
    public $css = [
        'css/shedule.css',
    ];
    
    public $js = [
        'js/shedule.js',
    ];
    
    public $depends = [
        'yii\jui\JuiAsset',
    ];
    
}