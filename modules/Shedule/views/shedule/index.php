<?php

use yii\helpers\Html;
use app\components\CGridView\CGridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Shedule\models\SheduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Shedules');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shedule-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Shedule'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div class="row">
        <div class="col-md-6">
            <?php Pjax::begin(); ?>    
                <?= CGridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'ID',
                        'Name',
                        [
                            'attribute' => 'Days',
                            'filter' => Html::activeInput('number', $searchModel, 'Days', ['class' => 'form-control']),
                            'width' => '150px'
                        ],

                        [
                            'class' => 'app\components\CGridView\CActionColumn'
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
    
</div>
