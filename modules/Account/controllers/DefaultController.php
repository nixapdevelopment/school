<?php

namespace app\modules\Account\controllers;


use app\controllers\FrontController;
use app\modules\Account\models\Account;
use app\modules\Lesson\models\Lesson;
use app\modules\Lesson\models\LessonTeacher;
use app\modules\Mark\models\MarkListSearch;
use app\modules\Pupil\models\Pupil;
use app\modules\Mark\models\Mark;
use app\modules\Shedule\models\Shedule;
use app\modules\Shedule\models\SheduleItem;
use app\modules\Teacher\models\Teacher;

class DefaultController extends FrontController
{

    public function actionIndex()
    {
        $account = Account::find()->where(['UserID'=>\Yii::$app->user->identity->ID])->with(['pupil','user'])->one();
        return $this->render('index',[
            'account' => $account
        ]);
    }

    public function actionMarks($lessonID)
    {
        $pupil = Pupil::find()->where(['ID' => \Yii::$app->user->identity->EntityID])->one();
        $lesson = Lesson::findone($lessonID);
        $lessonTeacher = LessonTeacher::find()->where(['LessonID'=>$lessonID])->one();
        $teacher = Teacher::findOne($lessonTeacher->TeacherID);
        $clasaID = $pupil->ClasaID;
        $shedule = Shedule::find()->where(['Status' => Shedule::StatusActive])->one();
        $sheduleItems = SheduleItem::find()->where(['SheduleID' => $shedule->ID, 'LessonID' => $lessonID, 'ClasaID' => $clasaID])->indexBy('Day')->all();
        $marks = Mark::find()->where(['LessonID' => $lessonID,'PupilID'=>$pupil->ID])->all();

        $marksGrouped = [];
        foreach ($marks as $mark)
        {
            $marksGrouped[$mark->PupilID][$mark->Date] = $mark;
        }

        $maxDate = strtotime($shedule->EndDate);

        if ($maxDate > strtotime('+ 1 month'))
        {
            $maxDate = strtotime('+ 1 month');
        }

        $dates = [];
        for ($date = strtotime($shedule->StartDate); $date <= $maxDate; $date += 86400)
        {
            $day = date('w', $date);
            if (isset($sheduleItems[$day]))
            {
                $dates[$date] = [
                    'systemDate' => date('Y-m-d', $date),
                    'fullDate' => date('d.m.Y', $date),
                    'shortDate' => date('d.m', $date),
                ];
            }
        }
        return $this->render('marks',[
            'pupil' => $pupil,
            'shedule' => $shedule,
            'sheduleItems' => $sheduleItems,
            'marks' => $marks,
            'dates' => $dates,
            'marksGrouped' => $marksGrouped,
            'lesson' => $lesson,
            'teacher' => $teacher
        ]);
    }

    public function actionMarksList(){

        $pupil = Pupil::find()->where(['ID' => \Yii::$app->user->identity->EntityID])->one();
        $searchModel = new MarkListSearch();
        $dataProvider = $searchModel->search([\Yii::$app->request->queryParams,['ClasaID'=>$pupil->ClasaID]]);

        return $this->render('markList', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'pupil' => $pupil,
        ]);
    }
}
