<?php
use app\modules\Mark\models\Mark;
?>

<div class="table-responsive">
    <table class="table table-bordered table-marks">
        <thead>
            <tr>
                <th>

                </th>
                <?php foreach ($dates as $timestamp => $date) { ?>
                <th class="text-center">
                    <span>
                        <?= $date['shortDate'] ?>
                    </span>
                </th>
                <?php } ?>
                <th>
                    Medie
                </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pupils as $pupil) { ?>
            <tr>
                <td style="padding: 2px 5px; vertical-align: middle;">
                    <div style="width: 140px;">
                        <?= $pupil->fullName ?>
                    </div>
                </td>
                <?php foreach ($dates as $timestamp => $date) { ?>
                <td style="padding: 2px;" class="editable-cell">
                    <input data-pupil-id="<?= $pupil->ID ?>" data-date="<?= $date['systemDate'] ?>" class="mark-input" value="<?= isset($marksGrouped[$pupil->ID][$date['systemDate']]) ? $marksGrouped[$pupil->ID][$date['systemDate']]->Mark : '' ?>" />
                </td>
                <?php } ?>
                <td style="padding: 2px;">
                    <input data-pupil-id="<?= $pupil->ID ?>" value="<?=number_format( Mark::find()->where([
                        'PupilID' => $pupil->ID,
                        'LessonID' => Yii::$app->request->get('lessonID'),
                        'Type' => 'Mark',
                    ])->average('Mark'), 2);?>" class="mark-input average-input" disabled />
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<?php $this->registerJs("
    
    $('table .editable-cell input').change(function(){
        $.post('" . \yii\helpers\Url::to(['save']) . "', {PupilID: $(this).attr('data-pupil-id'), Date: $(this).attr('data-date'), LessonID: " . Yii::$app->request->get('lessonID') . ", Mark: $(this).val()}, function(json){
            $('.average-input[data-pupil-id=' + json.model.PupilID + ']').val(json.average);
        }, 'json');
    });
    
    $('.mark-input').change(function(){
        var pupilID = $(this).attr('data-pupil-id');
    });
    
    $('.mark-input').keyup(function(e){
        if (e.which == 13)
        {
            if(e.shiftKey)
            {
                $(this).closest('tr').prev('tr').find('.mark-input[data-date=\"' + $(this).attr('data-date') + '\"]').focus();
            }
            else
            {
                $(this).closest('tr').next('tr').find('.mark-input[data-date=\"' + $(this).attr('data-date') + '\"]').focus();
            }
            
        }
    });
", yii\web\View::POS_READY) ?>

<?php $this->registerCss("
    .mark-input {
        width: 100%;
        border: none;
        height: 30px;
        text-align: center;
    }
"); ?>