<?php

namespace app\modules\Mark\models;

use yii\base\Model;
use app\modules\Lesson\models\Lesson;


class MarkList extends Model
{
    
    public $ID;
    
    public $SubjectID;
    
    public $Subject;
    
    public $ClasaID;
    
    public $Clasa;
    
    
    public function getList($params)
    {
        $query = Lesson::find()->joinWith(['subject', 'lessonClasas', 'lessonClasas.clasa', 'lessonTeachers', 'lessonTeachers.teacher']);
        
        if (!empty($params['ClasaID']))
        {
            $query->andWhere(['Clasa.ID' => $params['ClasaID']]);
        }
        
        if (!empty($params['SubjectID']))
        {
            $query->andWhere(['Subject.ID' => $params['SubjectID']]);
        }
        
        $lessons = $query->all();
        
        $list = [];
        foreach ($lessons as $lesson)
        {
            foreach ($lesson->lessonClasas as $lessonClasa)
            {
                $list[] = new MarkList([
                    'ID' => $lesson->ID,
                    'Subject' => $lesson->subject->Name,
                    'SubjectID' => $lesson->subject->ID,
                    'Clasa' => $lessonClasa->clasa->fullName,
                    'ClasaID' => $lessonClasa->clasa->ID,
                ]);
            }
        }
        
        return $list;
    }
    
}