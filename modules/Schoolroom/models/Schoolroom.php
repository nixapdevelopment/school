<?php

namespace app\modules\Schoolroom\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Schoolroom".
 *
 * @property integer $ID
 * @property string $Name
 * @property integer $Number
 * @property integer $Capacity
 */
class Schoolroom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Schoolroom';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Number', 'Capacity'], 'required'],
            [['Number', 'Capacity'], 'integer'],
            [['Name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Name' => Yii::t('app', 'Name'),
            'Number' => Yii::t('app', 'Number'),
            'Capacity' => Yii::t('app', 'Capacity'),
        ];
    }
    
    public static function getList($addEmpty = false)
    {
        $result = $addEmpty ? ['' => $addEmpty] : [];
        $list = ArrayHelper::map(Schoolroom::find()->all(), 'ID', 'Number');
        return $result + $list;
    }
    
}