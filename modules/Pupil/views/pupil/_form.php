<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Clasa\models\Clasa;
use app\components\CDatepicker\CDatepicker;
use app\modules\Pupil\models\Pupil;

/* @var $this yii\web\View */
/* @var $model app\modules\Pupil\models\Pupil */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pupil-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'FirstName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'LastName')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'BirthDate')->widget(CDatepicker::className(), [
                'options' => [
                    'value' => empty($model->BirthDate) ? date('d.m.Y') : $model->NiceDate
                ]
            ]) ?>
        </div>
        <div class="col-md-1">
            <?= $form->field($model, 'Gender')->dropDownList(Pupil::getGenderList(true)) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'ClasaID')->dropDownList(Clasa::getClassesList(true)) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'Phone')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
