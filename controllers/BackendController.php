<?php

namespace app\controllers;

use Yii;
use app\controllers\CController;

class BackendController extends CController
{

    public function init()
    {
        parent::init();

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (Yii::$app->user->identity->Type != 'Admin') {
            return $this->redirect('/account');
        }
        
        $this->layout = 'backend';
        
        Yii::$container->set('yii\widgets\Pjax', ['timeout' => 15000]);
    }

}