<?php

namespace app\modules\Lesson\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Lesson\models\Lesson;

/**
 * LessonSearch represents the model behind the search form about `app\modules\Lesson\models\Lesson`.
 */
class LessonSearch extends Lesson
{
    
    public $ClasaID;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'SubjectID', 'Lessons'], 'integer'],
            [['Type', 'ClasaID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lesson::find()->joinWith(['subject', 'lessonClasas', 'lessonClasas.clasa'])->groupBy('ID');

        // add conditions that should always apply here
        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'SubjectID' => $this->SubjectID,
            'Lessons' => $this->Lessons,
        ]);
        
        if ($this->ClasaID)
        {
            $query->andFilterWhere([
                'ClasaID' => $this->ClasaID,
            ]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
