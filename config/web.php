<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'language' => 'ro',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '3PXJDm0GI7yFk53Btm6fbZ1uH5N2NJt2',
            'baseUrl' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager' => [
            //'forceCopy' => true
        ],
        'user' => [
            'identityClass' => 'app\models\User\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [


                'account/marks' => 'account/default/marks-list',
                'account/subject' => '/account/default/marks',
            ]
        ],

        'view' => [
            'theme' => [
                'basePath' => '@app/views/themes/school',
                'pathMap' => [
                    '@app/views' => '@app/views/themes/school',
                ],
            ],
        ],
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'user' => [
            'class' => 'app\modules\User\User',
            'defaultRoute' => 'user/index'
        ],
        'account' => [
            'class' => 'app\modules\Account\Account',
            'defaultRoute' => 'default/index'
        ],
        'admin' => [
            'class' => 'app\modules\Admin\Admin',
            'defaultRoute' => 'default/index',
            'modules' => [
                'clasa' => [
                    'class' => 'app\modules\Clasa\Clasa',
                    'defaultRoute' => 'clasa/index'
                ],
                'pupil' => [
                    'class' => 'app\modules\Pupil\Pupil',
                    'defaultRoute' => 'pupil/index'
                ],
                'teacher' => [
                    'class' => 'app\modules\Teacher\Teacher',
                    'defaultRoute' => 'teacher/index'
                ],
                'subject' => [
                    'class' => 'app\modules\Subject\Subject',
                    'defaultRoute' => 'subject/index'
                ],
                'schoolroom' => [
                    'class' => 'app\modules\Schoolroom\Schoolroom',
                    'defaultRoute' => 'schoolroom/index'
                ],
                'shedule' => [
                    'class' => 'app\modules\Shedule\Shedule',
                    'defaultRoute' => 'shedule/index',
                ],
                'lesson' => [
                    'class' => 'app\modules\Lesson\Lesson',
                    'defaultRoute' => 'lesson/index',
                ],
                'mark' => [
                    'class' => 'app\modules\Mark\Mark',
                    'defaultRoute' => 'mark/index',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
