<?php

namespace app\modules\Teacher\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Teacher\models\Teacher;

/**
 * TeacherSearch represents the model behind the search form about `app\modules\Teacher\models\Teacher`.
 */
class TeacherSearch extends Teacher
{
    
    public $SubjectID = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ClasaID', 'RoomID'], 'integer'],
            [['FirstName', 'LastName', 'FatherName', 'BirthDate', 'Gender', 'City', 'Street', 'HouseNumber', 'Phone', 'Email', 'Level', 'IDNP', 'CPAS', 'CNAM', 'Speciality', 'BirthLocation', 'MilitaryStatus', 'RecruitmentDate', 'RecruitmentDocument', 'RecruitmentContract', 'SubjectID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Teacher::find()->with(['clasa'])->joinWith('teacherSubjects');

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'BirthDate' => $this->BirthDate,
            'ClasaID' => $this->ClasaID,
            'RoomID' => $this->RoomID,
            'RecruitmentDate' => $this->RecruitmentDate,
        ]);

        $query->andFilterWhere(['like', 'FirstName', $this->FirstName])
            ->andFilterWhere(['like', 'LastName', $this->LastName])
            ->andFilterWhere(['like', 'FatherName', $this->FatherName])
            ->andFilterWhere(['like', 'Gender', $this->Gender])
            ->andFilterWhere(['like', 'City', $this->City])
            ->andFilterWhere(['like', 'Street', $this->Street])
            ->andFilterWhere(['like', 'HouseNumber', $this->HouseNumber])
            ->andFilterWhere(['like', 'Phone', $this->Phone])
            ->andFilterWhere(['like', 'Email', $this->Email])
            ->andFilterWhere(['like', 'Level', $this->Level])
            ->andFilterWhere(['like', 'IDNP', $this->IDNP])
            ->andFilterWhere(['like', 'CPAS', $this->CPAS])
            ->andFilterWhere(['like', 'CNAM', $this->CNAM])
            ->andFilterWhere(['like', 'Speciality', $this->Speciality])
            ->andFilterWhere(['like', 'BirthLocation', $this->BirthLocation])
            ->andFilterWhere(['like', 'MilitaryStatus', $this->MilitaryStatus])
            ->andFilterWhere(['like', 'RecruitmentDocument', $this->RecruitmentDocument])
            ->andFilterWhere(['like', 'RecruitmentContract', $this->RecruitmentContract]);
        
        if ($this->SubjectID)
        {
            $query->andFilterWhere(['SubjectID' => $this->SubjectID]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
