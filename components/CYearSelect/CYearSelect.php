<?php

namespace app\components\CYearSelect;

use \etsoft\widgets\YearSelectbox;
use yii\helpers\Html;

class CYearSelect extends YearSelectbox
{
    
    public $addEmptyValue = false;
    
    public function run()
    {
        if ($this->hasModel()) {
            return Html::activeDropDownList($this->model, $this->attribute, $this->getItems(), $this->options);
        } else {
            return Html::dropDownList($this->name, $this->value, $this->getItems(), $this->options);
        }
    }
    
    public function getItems()
    {
        $typesAvailable = ['fix', 'calculation'];

        $this->yearStart = intval($this->yearStart);
        $this->yearEnd = intval($this->yearEnd);

        if (!in_array($this->yearStartType, $typesAvailable)) throw new InvalidConfigException("The 'yearStartType' option is must be: 'fix' or 'calculation'.");
        if (!in_array($this->yearEndType, $typesAvailable)) throw new InvalidConfigException("The 'yearEndType' option is must be: 'fix' or 'calculation'.");

        if ($this->yearStartType == 'calculation') $this->yearStart += date('Y');
        if ($this->yearEndType == 'calculation') $this->yearEnd += date('Y');

        $yearsRange = range($this->yearStart, $this->yearEnd);

        $result = $this->addEmptyValue ? ['' => '-'] : [];
        
        $result += array_combine($yearsRange, $yearsRange);
        
        return $result;
    }
    
}