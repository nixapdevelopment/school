<?php

namespace app\modules\Lesson\models;

use Yii;
use app\modules\Subject\models\Subject;
use app\modules\Lesson\models\LessonClasa;
use app\modules\Lesson\models\LessonSchoolroom;
use app\modules\Lesson\models\LessonTeacher;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "Lesson".
 *
 * @property integer $ID
 * @property integer $SubjectID
 * @property integer $Lessons
 * @property integer $MaxSheduleLessons
 * @property string $Type
 *
 * @property Subject $subject
 * @property LessonClasa[] $lessonClasas
 * @property LessonSchoolroom[] $lessonSchoolrooms
 * @property LessonTeacher[] $lessonTeachers
 */
class Lesson extends \yii\db\ActiveRecord
{
    
    const Type1 = 1;
    const Type2 = 2;
    const Type3 = 3;
    
    public static $TypesList = [
        self::Type1 => 'Ora',
        self::Type2 => 'Doua ore',
        self::Type3 => 'Trei ore',
    ];
    
    public $TeacherIDs = [];

    public static function getTypesList($addEmptyValue = false)
    {
        $result = $addEmptyValue ? ['' => '-'] : [];
        
        $result += self::$TypesList;
        
        return $result;
    }
    
    public static function getTypeName($type)
    {
        return self::$TypesList[$type];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Lesson';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SubjectID', 'Lessons', 'Type', 'MaxSheduleLessons'], 'required'],
            [['SubjectID', 'Lessons', 'MaxSheduleLessons', 'UsedLessons'], 'integer'],
            [['Type'], 'string', 'max' => 20],
            ['_TeacherIDs', 'safe'],
            [['SubjectID'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['SubjectID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SubjectID' => Yii::t('app', 'Subject ID'),
            'Lessons' => Yii::t('app', 'Lessons'),
            'Type' => Yii::t('app', 'Type'),
            'MaxSheduleLessons' => Yii::t('app', 'Max Shedule Lessons'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subject::className(), ['ID' => 'SubjectID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonClasas()
    {
        return $this->hasMany(LessonClasa::className(), ['LessonID' => 'ID'])->joinWith('clasa')->orderBy('Clasa.Number');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonSchoolrooms()
    {
        return $this->hasMany(LessonSchoolroom::className(), ['LessonID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonTeachers()
    {
        return $this->hasMany(LessonTeacher::className(), ['LessonID' => 'ID']);
    }
    
    public function getTeacherIDs()
    {
        return ArrayHelper::getColumn($this->lessonTeachers, 'TeacherID');
    }
    
    public function getClasasIDs()
    {
        return ArrayHelper::getColumn($this->lessonClasas, 'ClasaID');
    }
    
    public function getClasasNames()
    {
        return implode(', ', ArrayHelper::getColumn($this->lessonClasas, 'clasa.fullName'));
    }
    
    public function getSchoolroomIDs()
    {
        return ArrayHelper::getColumn($this->lessonSchoolrooms, 'SchoolroomID');
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // save teachers
        LessonTeacher::deleteAll(['LessonID' => $this->ID]);
        foreach (Yii::$app->request->post('TeacherIDs', []) as $TeacherID)
        {
            $model = new LessonTeacher([
                'LessonID' => $this->ID,
                'TeacherID' => $TeacherID,
            ]);
            $model->save();
        }
        
        // save classes
        LessonClasa::deleteAll(['LessonID' => $this->ID]);
        foreach (Yii::$app->request->post('ClasasIDs', []) as $ClasaID)
        {
            $model = new LessonClasa([
                'LessonID' => $this->ID,
                'ClasaID' => $ClasaID,
            ]);
            $model->save();
        }
        
        // save schoolrooms
        LessonSchoolroom::deleteAll(['LessonID' => $this->ID]);
        foreach (Yii::$app->request->post('SchoolroomIDs', []) as $SchoolroomID)
        {
            $model = new LessonSchoolroom([
                'LessonID' => $this->ID,
                'SchoolroomID' => $SchoolroomID,
            ]);
            $model->save();
        }
    }
    
}
