<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Clasa\models\Clasa */

$this->title = Yii::t('app', 'Create Clasa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clase'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clasa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
