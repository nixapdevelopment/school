<?php

namespace app\modules\Account\models;

use Yii;
use app\modules\Pupil\models\Pupil;
use app\models\User\User;

/**
 * This is the model class for table "Account".
 *
 * @property integer $ID
 * @property integer $UserID
 * @property integer $PupilID
 * @property string $CreatedAt
 *
 * @property Pupil $pupil
 * @property User $user
 */
class Account extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'Account';
    }

    public function rules()
    {
        return [
            [['UserID', 'PupilID'], 'required'],
            [['UserID', 'PupilID'], 'integer'],
            [['CreatedAt'], 'safe'],
            [['PupilID'], 'exist', 'skipOnError' => true, 'targetClass' => Pupil::className(), 'targetAttribute' => ['PupilID' => 'ID']],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'UserID' => 'User ID',
            'PupilID' => 'Pupil ID',
            'CreatedAt' => 'Created At',
        ];
    }

    public function getPupil()
    {
        return $this->hasOne(Pupil::className(), ['ID' => 'PupilID']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }
}
