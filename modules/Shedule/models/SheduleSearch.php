<?php

namespace app\modules\Shedule\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Shedule\models\Shedule;

/**
 * SheduleSearch represents the model behind the search form about `app\modules\Shedule\models\Shedule`.
 */
class SheduleSearch extends Shedule
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'integer'],
            [['Name', 'Days'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shedule::find();

        // add conditions that should always apply here

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere(['like', 'Name', $this->Name]);
        $query->andFilterWhere(['Days' => $this->Days]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
