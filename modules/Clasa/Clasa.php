<?php

namespace app\modules\Clasa;

/**
 * clasa module definition class
 */
class Clasa extends \app\components\CModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Clasa\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
