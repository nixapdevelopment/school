<?php

return [
    'adminEmail' => 'admin@example.com',
    'date' => [
        'displayDateFormat' => 'd.m.Y',
    ]
];