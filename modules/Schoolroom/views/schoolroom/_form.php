<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Schoolroom\models\Schoolroom */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="schoolroom-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'Number')->textInput() ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'Capacity')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
