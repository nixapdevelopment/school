<?php

namespace app\modules\Shedule\models;

use app\modules\Lesson\models\Lesson;
use app\modules\Shedule\models\Shedule;
use app\modules\Clasa\models\Clasa;

use Yii;

/**
 * This is the model class for table "SheduleItem".
 *
 * @property integer $ID
 * @property integer $SheduleID
 * @property integer $LessonID
 * @property integer $ClasaID
 * @property integer $Day
 * @property integer $Time
 * @property string $UniqueID
 *
 * @property Shedule $shedule
 * @property Lesson $lesson
 * @property Clasa $clasa
 */
class SheduleItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SheduleItem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SheduleID', 'LessonID', 'ClasaID', 'Day', 'Time', 'UniqueID'], 'required'],
            [['SheduleID', 'LessonID', 'ClasaID', 'Day', 'Time'], 'integer'],
            [['SheduleID'], 'exist', 'skipOnError' => true, 'targetClass' => Shedule::className(), 'targetAttribute' => ['SheduleID' => 'ID']],
            [['LessonID'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['LessonID' => 'ID']],
            [['ClasaID'], 'exist', 'skipOnError' => true, 'targetClass' => Clasa::className(), 'targetAttribute' => ['ClasaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SheduleID' => Yii::t('app', 'Shedule ID'),
            'LessonID' => Yii::t('app', 'Lesson ID'),
            'ClasaID' => Yii::t('app', 'Clasa ID'),
            'Day' => Yii::t('app', 'Day'),
            'Time' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShedule()
    {
        return $this->hasOne(Shedule::className(), ['ID' => 'SheduleID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['ID' => 'LessonID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasa()
    {
        return $this->hasOne(Clasa::className(), ['ID' => 'ClasaID']);
    }
}
