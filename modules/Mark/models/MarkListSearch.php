<?php

namespace app\modules\Mark\models;

use Yii;
use yii\data\ArrayDataProvider;


class MarkListSearch extends MarkList
{
    
    public $SubjectID;
    
    public $ClasaID;
    
    public function attributeLabels()
    {
        return [
            'SubjectID' => Yii::t('app', 'Subject'),
            'ClasaID' => Yii::t('app', 'Clasa'),
        ];
    }

    public function search($params)
    {
//   for account filter
        if (isset($params[0])) {
            $par1 = $params[1];
            $params = $params[0];
            $params['MarkListSearch']['ClasaID']= $par1['ClasaID'];
        }
//********* for account filter

        $params = isset($params['MarkListSearch']) ? $params['MarkListSearch'] : [];

        if (!empty($params['ClasaID']))
        {
            $this->ClasaID = $params['ClasaID'];
        }
        
        if (!empty($params['SubjectID']))
        {
            $this->SubjectID = $params['SubjectID'];
        }
        
        $model = new MarkList();
        $query = $model->getList($params);
        
        return new ArrayDataProvider([
            'allModels' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
    }
    
}