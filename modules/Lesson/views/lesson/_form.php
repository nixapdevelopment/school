<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Subject\models\Subject;
use app\modules\Lesson\models\Lesson;
use app\components\CSelect2\CSelect2Multiple;
use app\modules\Teacher\models\Teacher;
use yii\helpers\ArrayHelper;
use app\modules\Clasa\models\Clasa;
use app\modules\Schoolroom\models\Schoolroom;

/* @var $this yii\web\View */
/* @var $model app\modules\Lesson\models\Lesson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lesson-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'SubjectID')->dropDownList(Subject::getList(true)) ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'Lessons')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'Type')->dropDownList(Lesson::getTypesList(true)) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'MaxSheduleLessons')->textInput(['type' => 'number']) ?>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Profesori</label>
                <?= CSelect2Multiple::widget([
                    'name' => 'TeacherIDs[]',
                    'data' => ArrayHelper::map(Teacher::find()->joinWith('teacherSubjects')->all(), 'ID', 'fullName'),
                    'value' => $model->getTeacherIDs()
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Clase</label>
                <?= CSelect2Multiple::widget([
                    'name' => 'ClasasIDs[]',
                    'data' => Clasa::getClassesList(),
                    'value' => $model->getClasasIDs()
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Auditorii</label>
                <?= CSelect2Multiple::widget([
                    'name' => 'SchoolroomIDs[]',
                    'data' => ArrayHelper::map(Schoolroom::find()->all(), 'ID', 'Number'),
                    'value' => $model->getSchoolroomIDs(),
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
