<?php 

    use yii\widgets\Pjax;

?>

<?php Pjax::begin(['id' => 'shedule-widget-pjax']) ?>
<div class="shedule-widget">
    
    <table class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th>
                   
                </th>
                <?php foreach ($model->activeDays as $day) { ?>
                <th class="text-center">Ziua <?= $day ?></th>
                <?php } ?>
            </tr>
            <tr>
                <th>
                   
                </th>
                <?php foreach ($model->activeDays as $day) { ?>
                    <th>
                        <table style="margin: 0;" class="table-bordered table table-condensed">
                            <tr>
                                <?php for ($i = 1; $i <= $model->MaxLessons; $i++) { ?>
                                <td class="text-center">
                                    <?= $i; ?>
                                </td>
                                <?php } ?>
                            </tr>
                        </table>
                    </th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($classes as $class) { ?>
            <tr>
                <td style="width: 100px;" class="text-center">
                    <?= $class->FullName ?>
                </td>
                <?php foreach ($model->activeDays as $day) { ?>
                <td style="width: <?= round(100 / count($model->activeDays), 2) ?>%">
                    <table style="margin: 0;" class="table-bordered table table-condensed">
                        <tr>
                            <?php for ($i = 1; $i <= $model->MaxLessons; $i++) { ?>
                            <td data-day="<?= $day ?>" data-time="<?= $i ?>" data-clasa-id="<?= $class->ID ?>" class="droppable" style="height: 30px; width: <?= round(100 / $model->MaxLessons, 2) ?>%">
                                <?php if (isset($sheduleItems[$class->ID][$day][$i])) { ?>

                                <?php
                                    $a = 0;
                                foreach ($sheduleItems[$class->ID][$day][$i] as $index => $sheduleItem){
                                    $a = $a+1;
                                    if($a <= 2){
                                    ?>
                                <span style="background-color: <?= $sheduleItems[$class->ID][$day][$i][$index]->lesson->subject->Color ?>" data-id="<?= $sheduleItems[$class->ID][$day][$i][$index]->ID ?>" data-entity="lesson" data-clasa-id="<?= implode(',', $sheduleItems[$class->ID][$day][$i][$index]->lesson->clasasIDs) ?>" data-subject-id="<?= $sheduleItems[$class->ID][$day][$i][$index]->lesson->subject->ID ?>" data-lesson-id="<?= $sheduleItems[$class->ID][$day][$i][$index]->lesson->ID ?>" class="badge badge-info"><?= $sheduleItems[$class->ID][$day][$i][$index]->lesson->subject->ShortName ?></span>
                                <?php }}} ?>
                            </td>
                            <?php } ?>
                        </tr>
                    </table>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    
    <div class="free-lessons droppable">
        <?php foreach ($lessons as $lesson) { ?>
        <?php if ($lesson->MaxSheduleLessons - $lesson->UsedLessons <= 0) continue; ?>
        <span data-toggle="tooltip" data-placement="top" title="Clasa: <?= $lesson->clasasNames ?>" style="background-color: <?= $lesson->subject->Color ?>" data-entity="lesson" data-subject-id="<?= $lesson->subject->ID ?>" data-clasa-id="<?= implode(',', $lesson->clasasIDs) ?>" data-lesson-id="<?= $lesson->ID ?>" class="badge badge-info"><?= $lesson->subject->ShortName ?>(<?= $lesson->MaxSheduleLessons - $lesson->UsedLessons ?>)</span>
        <?php } ?>
    </div>
    
</div>
<script>initSheduleJs()</script>
<?php Pjax::end() ?>