<?php

namespace app\models\User;

use Yii;

/**
 * This is the model class for table "User".
 *
 * @property integer $ID
 * @property string $Email
 * @property string $Password
 * @property string $AuthKey
 * @property string $Name
 * @property string $Type
 * @property string $Status
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    
    const StatusActive = 'Active';
    const StatusInactive = 'Inactive';
    
    const TypeAdmin = 'Admin';
    const TypePupil = 'Pupil';
    const TypeParent = 'Parent';
    const TypeUnit = 'Unit';
    
    const ScenarioAdd = 'add';
    const ScenarioEdit = 'edit';
    const ScenarioRegister = 'register';
    
    public $PasswordConfirmation;



    public static function getStatusList()
    {
        return [
            '' => '-',
            User::StatusActive => 'Активен',
            User::StatusInactive => 'Неактивен'
        ];
    }
    
    public static function getStatusLabel($status)
    {
        $statusList = self::getStatusList();
        return $statusList[$status];
    }
    
    public function beforeSave($insert)
    {
       parent::beforeSave($insert);


       $this->Password = md5($this->Password);

        return true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'User';
    }
    
//    public function scenarios()
//    {
//        $scenarios = parent::scenarios();
//
//        $scenarios[self::ScenarioAdd] = ['Email', 'Password', 'Name', 'Status'];
//        $scenarios[self::ScenarioEdit] = ['Email', 'Name', 'Status'];
//        $scenarios[self::ScenarioRegister] = ['Email', 'Password', 'Name', 'PasswordConfirmation'];
//
//        return $scenarios;
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Email', 'Password', 'Name', 'Status','EntityID'], 'safe'],
//            [['Email', 'Password', 'Name'], 'string', 'max' => 255],
//            [['AuthKey'], 'string', 'max' => 50],
//            [['Status'], 'string', 'max' => 20],

            [['Email'], 'string'],
            [['Pin'], 'default', 'value' => null],
            [['AuthKey'], 'default', 'value' => null],
//            ['PasswordConfirmation', 'compare', 'compareAttribute' => 'Password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'Email' => 'Email',
            'Password' => Yii::t('app', 'Parola'),
            'AuthKey' => 'Auth Key',
            'Name' => Yii::t('app', 'Nume'),
//            'PasswordConfirmation' => Yii::t('app', 'Confirma parola'),
            'Status' => 'Status',
            'EntityID'=>'EntityID',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return User::find()->where(['ID' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['AuthKey' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return User::findOne(['Email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->ID;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->AuthKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->AuthKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->Password === md5($password);
    }
    
    public function validatePin($pin)
    {
        return $this->Pin === $pin;
    }
    
}
