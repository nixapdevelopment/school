<?php

namespace app\modules\Pupil\controllers;

use Yii;
use app\controllers\BackendController;

/**
 * Default controller for the `pupil` module
 */
class DefaultController extends BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}
