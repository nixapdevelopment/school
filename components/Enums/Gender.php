<?php

namespace app\components\Enums;

class Gender
{
    
    const Male = 'M';
    const Female = 'F';

    public static function getList($addEmptyValue = false)
    {
        $result = $addEmptyValue ? ['' => '-'] : [];
        
        $result += [
            self::Male => 'M',
            self::Female => 'F',
        ];
        
        return $result;
    }
    
}
