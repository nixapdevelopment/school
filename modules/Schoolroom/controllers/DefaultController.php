<?php

namespace app\modules\Schoolroom\controllers;

use Yii;

/**
 * Default controller for the `schoolroom` module
 */
class DefaultController extends \app\controllers\BackendController
{
    
    public function init()
    {
        parent::init();
        
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');
    }
    
}