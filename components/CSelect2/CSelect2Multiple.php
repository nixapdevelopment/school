<?php

namespace app\components\CSelect2;

class CSelect2Multiple extends CSelect2
{
    
    public $pluginOptions = [
        'multiple' => true,
        'allowClear' => true,
    ];
    
}
