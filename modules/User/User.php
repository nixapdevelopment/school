<?php

namespace app\modules\User;

use Yii;
use app\components\CModule;


class User extends CModule
{

    public $controllerNamespace = 'app\modules\User\controllers';
    
    public $name = 'User module';


    public function init()
    {
        parent::init();
        
        $this->layoutPath = Yii::getAlias('@app/views/themes/school/layouts');
    }
    
}
