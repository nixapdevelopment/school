<?php

namespace app\modules\Lesson\models;

use Yii;
use app\modules\Lesson\models\Lesson;
use app\modules\Teacher\models\Teacher;

/**
 * This is the model class for table "LessonTeacher".
 *
 * @property integer $ID
 * @property integer $LessonID
 * @property integer $TeacherID
 *
 * @property Lesson $lesson
 * @property Teacher $teacher
 */
class LessonTeacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LessonTeacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['LessonID'], 'required'],
            [['LessonID', 'TeacherID'], 'integer'],
            [['LessonID'], 'exist', 'skipOnError' => true, 'targetClass' => Lesson::className(), 'targetAttribute' => ['LessonID' => 'ID']],
            [['TeacherID'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['TeacherID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'LessonID' => Yii::t('app', 'Lesson ID'),
            'TeacherID' => Yii::t('app', 'Teacher ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(Lesson::className(), ['ID' => 'LessonID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Teacher::className(), ['ID' => 'TeacherID']);
    }
}
