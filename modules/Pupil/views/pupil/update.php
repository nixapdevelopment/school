<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Pupil\models\Pupil */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pupil',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pupils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pupil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes,
    ]) ?>

</div>
