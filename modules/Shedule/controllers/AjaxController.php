<?php

namespace app\modules\Shedule\controllers;

use Yii;
use yii\web\Response;
use yii\db\Expression;
use app\modules\Lesson\models\Lesson;
use app\modules\Shedule\models\SheduleItem;

/**
 * AjaxController
 */
class AjaxController extends DefaultController
{
    
    public function init()
    {
        parent::init();
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function actionCreate()
    {
        $clasaIDs = explode(',', Yii::$app->request->post('ClasaID'));
        
        $uniqueID = md5(microtime(true));
        
        $models = [];
        foreach ($clasaIDs as $clasaID)
        {
            $model = new SheduleItem();
            $model->SheduleID = Yii::$app->request->post('SheduleID');
            $model->LessonID = Yii::$app->request->post('LessonID');
            $model->ClasaID = $clasaID;
            $model->Day = Yii::$app->request->post('Day');
            $model->Time = Yii::$app->request->post('Time');
            $model->UniqueID = $uniqueID;
            $model->save();
            
            $models = $model;
        }
        
        Lesson::updateAll(['UsedLessons' => new Expression('UsedLessons + 1')], ['ID' => $model->LessonID]);
        
        return [
            'models' => $models,
        ];
    }
    
    public function actionUpdate()
    {
        $model = SheduleItem::findOne(Yii::$app->request->post('ID'));

        SheduleItem::updateAll([
            'Day' => Yii::$app->request->post('Day'),
            'Time' => Yii::$app->request->post('Time'),
        ], ['UniqueID' => $model->UniqueID]);
            
        return [
            'model' => $model,
        ];
    }
    
    
    public function actionDelete()
    {
        $model = SheduleItem::findOne(Yii::$app->request->post('ID'));
        
        SheduleItem::deleteAll(['UniqueID' => $model->UniqueID]);
        
        Lesson::updateAll(['UsedLessons' => new Expression('UsedLessons - 1')], ['ID' => $model->LessonID]);
        
        return [
            'ID' => Yii::$app->request->post('ID')
        ];
    }
    
}