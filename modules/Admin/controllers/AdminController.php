<?php

namespace app\modules\Admin\controllers;

use Yii;
use app\models\User\User;
use app\controllers\BackendController;
use yii\helpers\Url;

/**
 * Main controller for the `Admin` module
 */
class AdminController extends BackendController
{
   
    public function init()
    {
        $this->layout = Yii::getAlias('../../../../views/themes/school/layouts/backend');

        if (Yii::$app->user->identity->Type != User::TypeAdmin)
        {
          return $this->goHome();
        }

    }
    
}
